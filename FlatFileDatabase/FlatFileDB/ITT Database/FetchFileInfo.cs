using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace ITT.Database
{
    /// <summary>
    /// This class fetches information of file
    /// </summary>
    public class FetchFileInfo<T>
    {
        /// <summary>
        /// Get the absolute file name
        /// </summary>
        /// <typeparam name="T"></typeparam>+
        /// <param name="obj"></param>
        /// <returns>The file name</returns>
        public static string GetFileName(T obj)
        {
            string fileName = obj.GetType().ToString();
            return null == fileName ? throw new ArgumentNullException("fileName", "Name of the file in which the object is to be searched should not be null") : fileName;
        }

        /// <summary>
        /// Get the absolute file path
        /// </summary>
        /// <typeparam name="T"></typeparam>+
        /// <param name="fileName"></param>
        /// <returns>The file path</returns>
        public static string GetFilePath(string fileName)
        {
            string dbDirectory = @"D:/L&C/learnandcode/FlatFileDatabase/FlatFileDB/DBGenratedFiles/";
            return dbDirectory + fileName + ".txt";
        }
    }
}