using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;

namespace ITT.Database
{
    public class Deserializer<T>
    {
        public List<T> Deserialize(string serializedData)
        {
            return JsonConvert.DeserializeObject<List<T>>(serializedData);
        }
    }
}