using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace ITT.Database
{
    /// <summary>
    /// This class manage all file related operations
    /// </summary>
    public class FileIO<T>
    {
        /// <summary>
        /// Read the object list from file
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns> List of objects </returns>
        public static List<T> ReadFile(string filePath)
        {
            string serializedData;
            Deserializer<T> deserializer = new Deserializer<T>();
            using (StreamReader sr = new StreamReader(filePath))
            {
                serializedData = sr.ReadLine();
            }
            return deserializer.Deserialize(serializedData);
        }

        /// <summary>
        /// Write object list into file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectList"></param>
        /// <param name="filePath"></param>
        public static void WriteFile(List<T> objectList, string filePath)
        {
            Serializer<T> serializer = new Serializer<T>();
            string serializedData = serializer.Serialize(objectList);
            using (StreamWriter writer = File.CreateText(filePath))
            {
                // Add some text to file
                writer.WriteLine(serializedData);
            };
        }
    }
}
