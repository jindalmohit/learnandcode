using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;  
using System.Threading;

namespace ITT.Database
{
    /// <summary>
    /// Class ITTDB contains implementaion of CRUD operations.
    /// </summary>
    class ITTDB<T> : IITTDB<T>
    {
        private List<T> objectList;

        /// <summary>
        /// Save the object into file 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSave"></param>
        public void Save(T objectToSave)
        {
            ITTDB<T> self = this;
            if (objectToSave == null)
            {
                throw new ArgumentNullException("objectToSave", "object to be saved should not be null");
            }
            string fileName = FetchFileInfo<T>.GetFileName(objectToSave);
            string filePath = FetchFileInfo<T>.GetFilePath(fileName);
            objectList = (File.Exists(filePath)) ? FileIO<T>.ReadFile(filePath) : new List<T>();
            self.SetObjectId(objectList, objectToSave);
            objectList.Add(objectToSave);
            FileIO<T>.WriteFile(objectList, filePath);
        }

        /// <summary>
        /// Find the object present on specified file, if any
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectInstance"></param>
        /// <param name="Key"></param>
        /// <param name="expectedValue"></param>
        /// <returns>Matching object</returns>
        public T Find(string objectType, string key, string expectedValue)
        {
            T foundedObject;
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;  
            TextInfo textInfo = cultureInfo.TextInfo;
            string fileName = "Poco." + textInfo.ToTitleCase(objectType);
            string filePath = FetchFileInfo<T>.GetFilePath(fileName);
            if (File.Exists(filePath))
            {
                objectList = FileIO<T>.ReadFile(filePath);
                foundedObject = objectList.Find(obj => obj.GetType().GetProperty(key).GetValue(obj).ToString() == expectedValue);
                return null == foundedObject ? default(T) : foundedObject;
            }
            return default(T);
        }

        /// <summary>
        /// Delete the object from specified file, if any
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectInstance"></param>
        /// <param name="Key"></param>
        /// <param name="expectedValue"></param>
        /// <returns> true or false</returns>
        public bool Delete(T objectInstance, string key, string expectedValue)
        {
            T objectToDelete;
               string fileName = FetchFileInfo<T>.GetFileName(objectInstance);
                string filePath = FetchFileInfo<T>.GetFilePath(fileName);
                if (File.Exists(filePath))
                {
                    objectList = FileIO<T>.ReadFile(filePath);
                    objectToDelete = objectList.Find(obj => obj.GetType().GetProperty(key).GetValue(obj).ToString() == expectedValue);
                    if (null != objectToDelete)
                    {
                        objectList.Remove(objectToDelete);
                        FileIO<T>.WriteFile(objectList, filePath);
                        return true;
                    }
                }
            return false;
        }

        /// <summary>
        /// Edit the object in specified file, if any
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modifiedObject"></param>
        /// <returns> true or false </returns>
        public bool Edit(T modifiedObject)
        {
            T objectToModify;
            if (modifiedObject == null)
            {
                throw new ArgumentNullException("modifiedObject", "Object to be modified should not be null");
            }
            string fileName = FetchFileInfo<T>.GetFileName(modifiedObject);
            string filePath = FetchFileInfo<T>.GetFilePath(fileName);
        
            if (File.Exists(filePath))
            {
                objectList = FileIO<T>.ReadFile(filePath);
                string idOfOjbectToModify = modifiedObject.GetType().GetProperty("id").GetValue(modifiedObject).ToString();
                objectToModify = objectList.Find(obj => obj.GetType().GetProperty("id").GetValue(obj).ToString() == idOfOjbectToModify);
                if (null != objectToModify)
                {
                    int indexOfObjectToModify = objectList.IndexOf(objectToModify);
                    objectList.Remove(objectToModify);
                    objectList.Insert(indexOfObjectToModify, modifiedObject);
                    FileIO<T>.WriteFile(objectList, filePath);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Set the Id of newly created object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectList"></param>
        /// <param name="objectToSave"></param>
        private void SetObjectId(List<T> objectList, T objectToSave)
        {
            PropertyInfo objectIdProperty = objectToSave.GetType().GetProperty("id");
            if (objectList.Count == 0)
            {
                objectIdProperty.SetValue(objectToSave, 1, null);
            }
            else
            {
                object lastObject = objectList[objectList.Count-1];
                PropertyInfo lastObjectIdProperty = lastObject.GetType().GetProperty("id");
                int lastObjectId = Convert.ToInt32(lastObjectIdProperty.GetValue(lastObject).ToString());
                objectIdProperty.SetValue(objectToSave, lastObjectId+1, null);
            }
        }
    }
}