using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace ITT.Database
{
    public class ExceptionLogger
    {
        public static void LogException(Exception e)
        {
            string logFilePath = @"D:/L&C/learnandcode/FlatFileDatabase/FlatFileDB/obj/Debug/";
            using (StreamWriter logFile = new StreamWriter(logFilePath, true))
            {
                logFile.WriteLine(e.Message);
                logFile.Close();
            }
        }
    }
}