namespace ITT.Database
{
    /// <summary>
    /// Hold the responsibility of database CRUD operations.
    /// </summary>
    public interface IITTDB<T>
    {
        void Save(T objectToSave);
        T Find(string objectType, string key, string expectedValue);
        bool Delete(T objectInstance, string key, string expectedValue);
        bool Edit(T modifiedObject);
    }
}