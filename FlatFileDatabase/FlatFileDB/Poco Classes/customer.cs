namespace Poco
{
    /// <summary>
    /// Class Customer
    /// </summary>
    class Customer
    {
        public int id { get; set; }

        public string name { get; set; }

        public string address { get; set; }

        public Customer(string name, string address)
        {
            this.name = name;
            this.address = address;
        }
    }
}