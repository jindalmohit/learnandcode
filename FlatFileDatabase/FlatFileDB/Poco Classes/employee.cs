namespace Poco
{
    /// <summary>
    /// Class Employee
    /// </summary>
    public class Employee
    {
        public int id { get; set; }

        public string name { get; set; }

        public string address { get; set; }

        public Employee(string name, string address)
        {
            this.name = name;
            this.address = address;
        }
    }
}