﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using ITT.Database;
using Poco;

namespace Main
{
    /// <summary>
    /// Class Program
    /// </summary>
    class Program
    {
        /// <summary>
        /// Entry point of application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                ITTDB<Customer> db = new ITTDB<Customer>();
                Customer customer1 = new Customer("Sagar", "Jaipur");
                Customer customer2 = new Customer("Nandita", "Jaipur");
                Customer customer3 = new Customer("Divya", "Sri Ganganagar");
                Customer customer4 = new Customer("Himanshi", "Kanpur");
                Customer customer5 = new Customer("Garima", "Udaipur");
                // db.Save(customer4);
                Console.WriteLine(db.Find("Customer", "id", "1").name);
                //Console.WriteLine(db.Delete(customer4, "id", "2"));
                // Customer customer6 = db.Find("Customer", "id", "1");
                // customer6.address = "Mansarovar, Jaipur(Raj.)";
                // Console.WriteLine(db.Edit(customer6));

                ITTDB<Employee> EmployeeDb = new ITTDB<Employee>();
                Employee employee1 = new Employee("Aditya", "Jaipur");
                Employee employee2 = new Employee("Harsh", "Jaipur");
                // EmployeeDb.Save(employee2);
                // Console.WriteLine(EmployeeDb.Delete(employee2, "id", "2"));
                Console.ReadLine();
            }
            catch(Exception e)
            {
                ExceptionLogger.LogException(e);
            }
        }
    }
}
