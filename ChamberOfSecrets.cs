﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Learn_Code
{
    public class ChamberOfSecrets
    {
        Queue<int> spiders = new Queue<int>();
        int numberOfWrongPowersEntered = 0;

        /*taking input of number of spiders and number of spiders to be selected
          taking input of power of spiders*/
        public static void Main()
        {
            int spiderNumber = 0, selectedSpiderNumber = 0;
            ChamberOfSecrets aragog = new ChamberOfSecrets();
            
            string[] numberAndSelections = Console.ReadLine().Split();
            spiderNumber = int.Parse(numberAndSelections[0]);
            selectedSpiderNumber = int.Parse(numberAndSelections[1]);
        
            int[] power = new int[spiderNumber];
            string[] powerValues = Console.ReadLine().Split();

            if ((selectedSpiderNumber >= 1 && selectedSpiderNumber <= 316) || (spiderNumber >= selectedSpiderNumber && spiderNumber <= selectedSpiderNumber * selectedSpiderNumber))
            {
                power = aragog.populateSpiderData(spiderNumber, selectedSpiderNumber, powerValues);
                aragog.showSpiderWithMaximumPower(selectedSpiderNumber, power, aragog);
            }
        }

        //converting string array into integer array and populating queue with indexes of all spiders
        public int[] populateSpiderData(int spiderNumber, int selectedSpiderNumber, string[] powerValues)
        {
            int[] power = new int[spiderNumber];
            numberOfWrongPowersEntered = 0;

            for (int index = 0; index < spiderNumber; index++)
            {
                power[index] = int.Parse(powerValues[index]);
                if (power[index] < 1 && power[index] > selectedSpiderNumber)
                    numberOfWrongPowersEntered++;
                spiders.Enqueue(index);
            }
            return power;
        }

        /* finding the maximum power and printing it's index
        eliminating index of maximum power from the queue */
        public void showSpiderWithMaximumPower(int selectedSpiderNumber, int[] spiderPower, ChamberOfSecrets aragog)
        {
            int iteration = 0;
            if (numberOfWrongPowersEntered == 0)
            {
                while (iteration < selectedSpiderNumber)
                {
                    int[] removedEntriesIndex = new int[selectedSpiderNumber + 1];
                    int sizeOfQueue = spiders.Count;
                    int maxPower = -1;
                    int maxIndex = -1;
                    for (int index = 0; index < Math.Min(selectedSpiderNumber, sizeOfQueue); index++)
                    {
                        removedEntriesIndex[index] = spiders.Dequeue();
                        if (spiderPower[removedEntriesIndex[index]] > maxPower)
                        {
                            maxPower = spiderPower[removedEntriesIndex[index]];
                            maxIndex = removedEntriesIndex[index];
                        }
                    }
                    Console.Write(maxIndex + 1 + " ");

                    aragog.decrementSpiderPowerByOne(selectedSpiderNumber, removedEntriesIndex, spiderPower, sizeOfQueue, maxIndex);
                   
                    iteration++;
                }
                Console.ReadLine();
            }
        }

        //decrementing remaining powers by 1
        public void decrementSpiderPowerByOne(int selectedSpiderNumber, int[] removedEntriesIndex, int[] spiderPower, int sizeOfQueue, int maxIndex)
        {
            for (int index = 0; index < Math.Min(selectedSpiderNumber, sizeOfQueue); index++)
            {
                if (removedEntriesIndex[index] != maxIndex)
                {
                    if (spiderPower[removedEntriesIndex[index]] != 0)
                        spiderPower[removedEntriesIndex[index]]--;
                    spiders.Enqueue(removedEntriesIndex[index]);
                }
            }
        }
    }
}
