using System; 
using System.Net; 
using System.Net.Sockets; 
using System.Text;
using Newtonsoft.Json;

namespace DBClient
{
    public class ConnectionHanlder
    {

		// Establish the remote endpoint 
		// for the socket. This example 
		// uses port 11111 on the local 
		// computer. 
		static IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
		static IPAddress ipAddr = ipHost.AddressList[0];
		IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);

		// Creation TCP/IP Socket using 
		// Socket Class Costructor 
		Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

		public void connect()
        {
			sender.Connect(localEndPoint);
			Console.WriteLine("Socket connected to -> {0} ", sender.RemoteEndPoint.ToString());
		}

		public void sendRequest(dynamic request)
        {
			string serializedRequest = JsonConvert.SerializeObject(request);
			Console.WriteLine("send request");
			Console.WriteLine(serializedRequest);
            byte[] messageSent = Encoding.ASCII.GetBytes(serializedRequest); 
            int byteSent = sender.Send(messageSent);
		}

		public string receiveResponse()
        {

			byte[] byteReceived = new byte[1024];
			//string messageReceived = null;

			int numberOfByteReceived = sender.Receive(byteReceived);
			string messageReceived = Encoding.ASCII.GetString(byteReceived, 0, numberOfByteReceived);
			Console.WriteLine("Message from Server -> {0}", messageReceived);
			//Console.ReadLine();
			return messageReceived; 
		}

		public void disconnect()
        {
			Console.WriteLine("tum b jao");
			sender.Shutdown(SocketShutdown.Both); 
			sender.Close();
		}
	}
}