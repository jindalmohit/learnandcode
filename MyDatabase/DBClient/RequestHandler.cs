using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;  
using System.Threading;
using Models;

namespace DBClient
{
    public class RequestHandler
    {
        public Request GetRequest()
        {
            Console.WriteLine("Enter the number for the operation you want to perform. \n Press");
            Console.WriteLine(" 1 to Find \n 2 to Save \n 3 to Update \n 4 to Delete");
            int operationId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(operationId);
            return GetOperationSpecificRequest(operationId);
        }

        private Request GetOperationSpecificRequest(int operationId)
        {
            Console.WriteLine(operationId);
            Request requestData = new Request();
            switch(operationId)
            {
                case 1: dynamic findData = GetRequestToFindOrDelete("Find");
                    return FormRequestObject("Find", findData);
                case 2: dynamic saveData = GetRequestToSave();
                    return FormRequestObject("Save", saveData);
                case 3: dynamic updateData = GetRequestToUpdate();
                    return FormRequestObject("Modify", updateData);
                case 4: dynamic deleteData = GetRequestToFindOrDelete("Delete");
                    return FormRequestObject("Delete", deleteData);
                default: Console.WriteLine("Invalid input");
                    return null;
            }
        }

        private Request FormRequestObject(string type, dynamic data)
        {
            Console.Write("FormRequestObject");
            Console.WriteLine(data);
            //string requestData = data.ToString().Replace("\"", "");
            Request requestObject = new Request();
            requestObject.Type = type;
            requestObject.Data = data;
            return requestObject;
        }

        private string ConvertToTitleCase(string text)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;  
            TextInfo textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(text);
        }

        private dynamic GetInstance(string fullyQualifiedClassName)
        {
            Type type = Type.GetType(fullyQualifiedClassName);
            if (type != null)
                return Activator.CreateInstance(type);
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = assembly.GetType(fullyQualifiedClassName);
                if (type != null)
                    return Activator.CreateInstance(type);
            }
            return null;
        }

        private dynamic GetRequestToFindOrDelete(string operationType)
        {
            Console.WriteLine("Enter the type of parameter by which you want to search or delete.");
            Console.WriteLine(" 1: Id \n 2: Name \n 3: Address");
            string key = ConvertToTitleCase(Console.ReadLine().ToString());

            Console.WriteLine("Enter the type of data you want to search or delete.");
            string objectType = ConvertToTitleCase(Console.ReadLine().ToString());

            Console.WriteLine("Enter the expected value");
            string expectedValue = ConvertToTitleCase(Console.ReadLine().ToString());

            FindOrDeleteRequest findData = new FindOrDeleteRequest();
            findData.ObjectType = objectType;
            findData.Key = key;
            findData.ExpectedValue = expectedValue;

            return JsonConvert.SerializeObject(findData);
        }

        private dynamic GetRequestToSave()
        {
            Console.WriteLine("Enter the type of object you want to save.");
            string objectType = ConvertToTitleCase(Console.ReadLine().ToString());
            Console.WriteLine(objectType);
            var obj = GetInstance("Models." + objectType);
            Console.WriteLine(obj);
            Console.WriteLine("Enter name");
            string name = ConvertToTitleCase(Console.ReadLine().ToString());
            Console.WriteLine("Enter Address");
            string address = ConvertToTitleCase(Console.ReadLine().ToString());

            obj.Name = name;
            obj.Address = address;
            Console.WriteLine(obj.Name);

            SaveRequest saveData = new SaveRequest();
            saveData.FileName = objectType;
            saveData.data = JsonConvert.SerializeObject(obj);

            Console.WriteLine(saveData.data);
            //Console.WriteLine(saveData.data.Address);
            

            return JsonConvert.SerializeObject(saveData);
        }

        private dynamic GetRequestToUpdate()
        {
            Console.WriteLine("Enter the type of parameter of the object you want to update.");
            Console.WriteLine(" 1: Id \n 2: Name \n 3: Address");
            string key = ConvertToTitleCase(Console.ReadLine().ToString());

            Console.WriteLine("Enter the expected value of the parameter");
            string expectedValue = ConvertToTitleCase(Console.ReadLine().ToString());

            Console.WriteLine("Enter the type of object you want to update.");
            string objectType = ConvertToTitleCase(Console.ReadLine().ToString());

            var obj = GetInstance("Models." + objectType);

            Console.WriteLine("Enter name");
            string name = ConvertToTitleCase(Console.ReadLine().ToString());

            Console.WriteLine("Enter Address");
            string address = ConvertToTitleCase(Console.ReadLine().ToString());

            obj.Name = name;
            obj.Address = address;

            UpdateRequest updateData = new UpdateRequest();
            updateData.Key = key;
            updateData.ExpectedValue = expectedValue;
            updateData.FileName = objectType;
            updateData.data = obj;

            return JsonConvert.SerializeObject(updateData);
        }
    }
}