using System; 
using System.Net; 
using System.Net.Sockets; 
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using Models;

namespace DBClient
{
    public class ClientManager
    {
		public void ExecuteClient() 
		{ 
			try
            {
                RequestHandler requestHandler = new RequestHandler();
                Request request = requestHandler.GetRequest();
				Console.WriteLine(request.Data);

                // dynamic serializedRequest = JsonConvert.SerializeObject(request);
				// Console.WriteLine(serializedRequest);

				ConnectionHanlder connectionHandler = new ConnectionHanlder();

				try
                {
					connectionHandler.connect();
					connectionHandler.sendRequest(request);
					string serverResponse = connectionHandler.receiveResponse();
					Console.WriteLine("kyu ni ho rhe ho");
					connectionHandler.disconnect();
				} 
				
				catch (ArgumentNullException ane)
                { 
					Console.WriteLine("ArgumentNullException : {0}", ane.ToString()); 
				} 
				
				catch (SocketException se)
                { 
					Console.WriteLine("SocketException : {0}", se.ToString());
					Console.ReadLine();
				} 
				
				catch (Exception e)
                { 
					Console.WriteLine("Unexpected exception : {0}", e.ToString());
					Console.ReadLine();
				} 
			} 
			
			catch (Exception e)
            { 
				Console.WriteLine(e.ToString());
				Console.ReadLine();
			} 
		} 
	}
}