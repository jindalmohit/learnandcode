namespace Models
{
    public class Request
    {
        public string Type { get; set; }
        public dynamic Data { get; set; }
    }

    public class FindOrDeleteRequest
    {
        public string ObjectType { get; set; }
        public string Key { get; set; }
        public string ExpectedValue { get; set; }
    }

    public class SaveRequest
    {
        public string FileName { get; set; }
        public dynamic data { get; set; }
    }

    public class UpdateRequest
    {
        public string FileName { get; set; }
        public string Key { get; set; }
        public string ExpectedValue { get; set; }
        public dynamic data { get; set; } 
    }
}