namespace Models
{
    public class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public Employee(string name, string address)
        {
            this.Name = name;
            this.Address = address;
        }
        public Employee(){}
    }
}