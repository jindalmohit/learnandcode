namespace Models
{
    /// <summary>
    /// Class Customer
    /// </summary>
    class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public Customer(string name, string address)
        {
            this.Name = name;
            this.Address = address;
        }
        public Customer(){}
    }
}