﻿using System;

namespace DBClient
{
    class Client
    {
        static void Main(string[] args)
        {
            ClientManager clientManager = new ClientManager();
            clientManager.ExecuteClient();
        }
    }
}
