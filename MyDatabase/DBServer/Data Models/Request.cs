namespace Models
{
    public class Request
    {
        public string Type { get; set; }
        public dynamic Data { get; set; }
    }
}