namespace Models
{
    public class Response
    {
        public string Status { get; set; }
        public string Error_message { get; set; }
        public dynamic Data { get; set; }
    }
}