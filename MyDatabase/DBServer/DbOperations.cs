using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;  
using System.Threading;

namespace DBServer
{
    public class DbOperations
    {
        private List<dynamic> objectList;
        private dynamic SetObjectId(dynamic objectToSave)
        {
           Random random = new Random();
           objectToSave.Id = random.Next();
           return objectToSave;
        }
        public dynamic Save(string fileName, dynamic objectToSave)
        {
            // Is ths file available to save
            // Check the status of file object in dictionary
            // if available, lock the dictionary and have this thread update the status of the file to lock it
            //ITTDB<T> self = this;
            if (objectToSave == null)
            {
                throw new ArgumentNullException("objectToSave", "object to be saved should not be null");
            }
            //string fileName = FetchFileInfo<T>.GetFileName(objectToSave);
            // while(!IsFileLocked(fileName)){
            //     // Update file
            // }
            //Type objectType = Type.GetType("Models." + fileName);
            string filePath = FetchFileInfo.GetFilePath(fileName);
            objectList = (File.Exists(filePath)) ? FileIO<dynamic>.ReadFile(filePath) : new List<dynamic>();
            dynamic saveData = SetObjectId(objectToSave);
            objectList.Add(saveData);
            FileIO<dynamic>.WriteFile(objectList, filePath);
            return saveData;
        }
// private void FreeUpFile(string fileName){
//     lock(){
//         // update the status to "available"
//     }
// }


//         private bool IsFileLocked(string fileName){
// lock(){
//     // Checkj if this file is available in the dictionary
//     // If yes, update its status to "in use"
// }

//         }

        public dynamic Find(string objectType, string key, string expectedValue)
        {
            dynamic foundedObject = null;
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;  
            TextInfo textInfo = cultureInfo.TextInfo;
            string fileName = textInfo.ToTitleCase(objectType);
            string filePath = FetchFileInfo.GetFilePath(fileName);
            if (File.Exists(filePath))
            {
                objectList = FileIO<dynamic>.ReadFile(filePath);
                foundedObject = objectList.Find(obj => obj.Property(key) != null && obj[key].ToString() == expectedValue);
            }
            return null == foundedObject ? default(dynamic) : foundedObject;
        }

        public bool Delete(string objectType, string key, string expectedValue)
        {
            dynamic objectToDelete = null;
            //string fileName = FetchFileInfo<T>.GetFileName(objectInstance);
            //Type objectType = Type.GetType("Models." + objectType);
            string filePath = FetchFileInfo.GetFilePath(objectType);
            if (File.Exists(filePath))
            {
                objectList = FileIO<dynamic>.ReadFile(filePath);
                objectToDelete = objectList.Find(obj => obj.Property(key) != null && obj[key].ToString() == expectedValue);
                if (null != objectToDelete)
                {
                    objectList.Remove(objectToDelete);
                    FileIO<dynamic>.WriteFile(objectList, filePath);
                    return true;
                }
            }
            return false;
        }
        
        public bool Edit(string objectType, string key, string expectedValue, dynamic modifiedObject)
        {
            dynamic objectToModify;
            if (modifiedObject == null)
            {
                throw new ArgumentNullException("modifiedObject", "Object to be modified should not be null");
            }
            //string fileName = FetchFileInfo<T>.GetFileName(modifiedObject);
            //Type objectType = Type.GetType("Models." + objectType);
            string filePath = FetchFileInfo.GetFilePath(objectType);
        
            if (File.Exists(filePath))
            {
                objectList = FileIO<dynamic>.ReadFile(filePath);
               // string idOfOjbectToModify = modifiedObject.GetType().GetProperty(key).GetValue(modifiedObject).ToString();
                objectToModify = objectList.Find(obj => obj.Property(key) != null && obj[key].ToString() == expectedValue);
                if (null != objectToModify)
                {
                    modifiedObject["Id"] = objectToModify["Id"];
                    int indexOfObjectToModify = objectList.IndexOf(objectToModify);
                    objectList.Remove(objectToModify);
                    objectList.Insert(indexOfObjectToModify, modifiedObject);
                    FileIO<dynamic>.WriteFile(objectList, filePath);
                    return true;
                }
            }
            return false;
        }
        
    }
}