using System; 
using System.Net; 
using System.Net.Sockets; 
using System.Text;
using Newtonsoft.Json;
using Models;

namespace DBServer
{
    public class ServerManager
    {
        public void ExecuteServer()
        {
            ConnectionHandler serverConnectionHandler = new ConnectionHandler();
            try {

				serverConnectionHandler.startServer();

				while (true) { 
					
					Console.WriteLine("Waiting for a connection ... "); 

					serverConnectionHandler.accept();

					HandleRequestAndResponse(serverConnectionHandler);

					serverConnectionHandler.closeClientSocket(); 
				} 
			} 
			
			catch (Exception e)
            {
                serverConnectionHandler.sendResponse(new RequestAndResponseHandler().CreateResponse("Failure", e.Message.ToString(), null));
				Console.WriteLine(e.ToString());
			}
        }

        private void HandleRequestAndResponse(ConnectionHandler serverConnectionHandler)
        {
            RequestAndResponseHandler handler = new RequestAndResponseHandler();
            byte[] bytesReceived = new Byte[1024]; 
            string messageReceived = null;
            
            string clientRequest = serverConnectionHandler.receiveAndProcessRequest(bytesReceived, messageReceived);
            Console.WriteLine("\nText received -> {0} ", clientRequest);

            Request request = JsonConvert.DeserializeObject<Request>(clientRequest);
            dynamic response = handler.GetResponse(request);
            Response serverResponse = handler.CreateResponse("Success", string.Empty, response);
            serverConnectionHandler.sendResponse(serverResponse);
        }
    }
}