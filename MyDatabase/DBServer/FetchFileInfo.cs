using System;

namespace DBServer
{
    public class FetchFileInfo
    {
        // public static string GetFileName(T obj)
        // {
        //     string fileName = obj.GetType().ToString();
        //     return null == fileName ? throw new ArgumentNullException("fileName", "Name of the file in which the object is to be searched should not be null") : fileName;
        // }
        public static string GetFilePath(string fileName)
        {
            string dbDirectory = @"D:/L&C/learnandcode/MyDatabase/DBServer/DBGeneratedFiles/";
            return dbDirectory + fileName + ".txt";
        }
    }
}