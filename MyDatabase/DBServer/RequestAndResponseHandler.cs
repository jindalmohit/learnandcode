using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;
using System.Globalization;  
using System.Threading;
using Models;

namespace DBServer
{
    public class RequestAndResponseHandler
    {
        public Response CreateResponse(string status,string errorMessage, dynamic responseFromServer)
        {
            Response response = new Response();
            response.Status = status;
            response.Error_message = errorMessage;
            response.Data = responseFromServer;
            return response;
        }

        public dynamic GetResponse(Request request)
        {
            DbOperations db = new DbOperations();
            switch(request.Type)
            {

                case "Find":
                    dynamic findData = JsonConvert.DeserializeObject<dynamic>(request.Data);
                    return db.Find(findData.ObjectType.ToString(), findData.Key.ToString(), findData.ExpectedValue.ToString());
                case "Save":
                    dynamic saveRequest = JsonConvert.DeserializeObject<dynamic>(request.Data);
                    dynamic saveData = JsonConvert.DeserializeObject<dynamic>(saveRequest.data.ToString());
                    return db.Save(saveRequest.FileName.ToString(), saveData);
                case "Modify":
                    dynamic updateRequest = JsonConvert.DeserializeObject<dynamic>(request.Data);
                    dynamic updateData = JsonConvert.DeserializeObject<dynamic>(updateRequest.data.ToString());
                    return db.Edit(updateRequest.FileName.ToString(), updateRequest.Key.ToString(), updateRequest.ExpectedValue.ToString(), updateData) ? "1 row affected" : "0 rows affected";
                case "Delete":
                    dynamic deleteData = JsonConvert.DeserializeObject<dynamic>(request.Data);
                    return db.Delete(deleteData.ObjectType.ToString(), deleteData.Key.ToString(), deleteData.ExpectedValue.ToString()) ? "1 row affected" : "0 rows affected";
                default:
                    Console.WriteLine("Invalid request type");
                    return null;
            }
        } 
    }
}