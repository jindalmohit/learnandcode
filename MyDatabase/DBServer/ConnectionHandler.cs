using System; 
using System.Net; 
using System.Net.Sockets; 
using System.Text;
using Newtonsoft.Json;

namespace DBServer
{
    public class ConnectionHandler
    {

		// Establish the local endpoint 
		// for the socket. Dns.GetHostName 
		// returns the name of the host 
		// running the application.
		static IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName()); 
		static IPAddress ipAddr = ipHost.AddressList[0]; 
		IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111); 

		// Creation TCP/IP Socket
		Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
		public Socket clientSocket;

		/// <summary>
		/// Bind or Associate a network address to the socket and 
		/// making it ready to listen to some number of clients
		/// which will form a waiting queue
		/// </summary>
		public void startServer() { 
			listener.Bind(localEndPoint); 
			listener.Listen(10);
		}

		/// <summary>
		/// Accept Client connection request
		/// </summary>
		public void accept() {
			clientSocket = listener.Accept();
		}

		/// <summary>
		/// Recieve and process request from client
		/// </summary>
		/// <typeparam1 name="byte[]"></typeparam1>
		/// <param1 name="bytesFromServer"></param1>
		/// <typeparam2 name="string"></typeparam2>
		/// <param2 name="message"></param2>
		public string receiveAndProcessRequest(byte[] bytesFromServer, string message = null)
        {
			string clientData = null;
			while (true) { 

				int numByte = clientSocket.Receive(bytesFromServer); 
				
				message = Encoding.ASCII.GetString(bytesFromServer, 0, numByte);
				Console.WriteLine(message);
				clientData += message;
											
				if (clientData != null) 
					break; 
			}
            Console.WriteLine(clientData);

			return clientData;
		}

		/// <summary>
		/// Send a message to Client
		/// </summary>
		public void sendResponse(dynamic serverResponse)
        {
            dynamic response = JsonConvert.SerializeObject(serverResponse, Formatting.Indented);
			byte[] message = Encoding.ASCII.GetBytes(response); 
			clientSocket.Send(message); 
		}

		/// <summary>
		/// Close client Socket
		/// </summary>
		public void closeClientSocket() {
			clientSocket.Shutdown(SocketShutdown.Both); 
			clientSocket.Close();
		}
	}
}