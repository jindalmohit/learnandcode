﻿using System;

namespace DBServer
{
    class Server
    {
        static void Main(string[] args)
        {
            ServerManager serverManager = new ServerManager();
            serverManager.ExecuteServer();
        }
    }
}
