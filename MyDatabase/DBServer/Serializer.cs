using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;

namespace DBServer
{
    public class Serializer<T>
    {
        public string Serialize(List<T> objectLsit)
        {
            return JsonConvert.SerializeObject(objectLsit);
        }
    }
}